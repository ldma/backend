#!/bin/bash
# Borra todas las lambdas tuyas personales (deja límpio tu api gateway + lambda),
#  Borra todas las lambdas ldma compartidas (deja límpio tu api gateway + lambda),
# borra tu api de la cuenta personal
# importa las lambdas nuevas y las sube y hace update de la api gateway
# Consideraciones: para que pueda exportar las lambda (en formato zip) no debes borrarlas de ~/Downloads

#invocar con el nombre que quieres updatear la descripción de la api 
# sh 03... "issue #50 solved"
# 
clear

export AWS_PROFILE="personal" && export AWS_REGION="eu-west-2"

function pause(){
   read -p "$*"
}

function getCredentials(){
    personalAWSAccountID=$(jq '.personalAWSAccountID' AWSConfigDeploy.json)
    sharedldmaAWSAccountID=$(jq '.sharedldmaAWSAccountID' AWSConfigDeploy.json)
    personalApiID=$(jq '.personalApiID' AWSConfigDeploy.json)
    sharedldmaApiID=$(jq '.sharedldmaApiID' AWSConfigDeploy.json)
    personalAWSAccountID=${personalAWSAccountID//[!0-9]/}
    sharedldmaAWSAccountID=${sharedldmaAWSAccountID//[!0-9]/}
    personalApiID=${personalApiID//[!0-9&!a-z]/}
    sharedldmaApiID=${sharedldmaApiID//[!0-9&!a-z]/}
}
getCredentials

#  borrar todas las lambdas cuenta personal
touch urlsLambdas
aws lambda list-functions --region=eu-west-2 > listLambdasAwsPersonal.json
countItems=$(jq '.Functions | length' listLambdasAwsPersonal.json)
echo You have $countItems lambda in $AWS_REGION.
if [ $countItems -gt "0" ]; then
    for (( c=0; c<$countItems; c++ )) do
        echo "Deleting All lambdas...\n See the 'eraseAllLambdaPersonal.json'" 
        lambdaName=$(jq '.Functions['$c'].FunctionName' listLambdasAwsPersonal.json) 
        lambdaName="${lambdaName//'"'}"
        aws lambda delete-function --function-name $lambdaName --region eu-west-2
        echo "lambda $(($c+1)) -> $lambdaName deleted!"
        done
fi

# borrar la api de la cuenta personal
echo "Deleting the API gateway associate .." # TODO add name or id
aws apigateway delete-rest-api --rest-api-id $personalApiID --region=eu-west-2

# borrar todas las lambda de la cuenta compartida
export AWS_PROFILE="ldma" && export AWS_REGION="eu-west-2"
touch urlsLambdas
aws lambda list-functions --region=eu-west-2 > listLambdasAwsLdma.json
countItems=$(jq '.Functions | length' listLambdasAwsLdma.json)
echo You have $countItems lambda in $AWS_REGION.
if [ $countItems -gt "0" ]; then
    for (( c=0; c<$countItems; c++ )) do
        echo "Deleting All lambdas...\n See the 'listLambdasAwsLdma.json'" 
        lambdaName=$(jq '.Functions['$c'].FunctionName' listLambdasAwsLdma.json) 
        lambdaName="${lambdaName//'"'}"
        aws lambda delete-function --function-name $lambdaName --region eu-west-2
        echo "lambda $(($c+1)) -> $lambdaName deleted!"
        done
fi

# importar las lambdas nuevas
export AWS_PROFILE="ldma" && export AWS_REGION="eu-west-2"

clear

# outputDirLambdas="LambdasZIPToPersonal"
# cd lambdas
cd ~/Downloads
for file in *.js; do 
     aws lambda create-function --function-name ldma-dev-${file%.*} --runtime nodejs6.10 --role arn:aws:iam::$sharedldmaAWSAccountID:role/ldma-dev-eu-west-2-lambdaRole --handler ${file%.*}.${file%.*} --zip-file fileb://${file%.*}.zip  --region=eu-west-2; 
     export statementIdUUID=`uuidgen`
     aws lambda add-permission --function-name ldma-dev-${file%.*}  --principal apigateway.amazonaws.com --statement-id  $statementIdUUID --action lambda:* --region=eu-west-2
done

aws apigateway update-rest-api --rest-api-id 5ktml9zak2 --patch-operations op=replace,path=/description,value=$1 --region=eu-west-2
# En caso de querer importar toda la api si no estuviera.
#aws apigateway import-rest-api --fail-on-warnings --body 'file://dev-ldma-API.json' --region=eu-west-2


