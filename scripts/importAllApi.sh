#!/bin/bash
# Para ejecutar este script debes tener un perfil
clear

export AWS_PROFILE="personal" && export AWS_REGION="eu-west-2"

function pause(){
   read -p "$*"
}

function getCredentials(){
    personalAWSAccountID=$(jq '.personalAWSAccountID' AWSConfigDeploy.json)
    sharedldmaAWSAccountID=$(jq '.sharedldmaAWSAccountID' AWSConfigDeploy.json)
    personalApiID=$(jq '.personalApiID' AWSConfigDeploy.json)
    sharedldmaApiID=$(jq '.sharedldmaApiID' AWSConfigDeploy.json)
    personalAWSAccountID=${personalAWSAccountID//[!0-9]/}
    sharedldmaAWSAccountID=${sharedldmaAWSAccountID//[!0-9]/}
    personalApiID=${personalApiID//[!0-9&!a-z]/}
    sharedldmaApiID=${sharedldmaApiID//[!0-9&!a-z]/}
}
getCredentials

# importar las lambdas nuevas
export AWS_PROFILE="ldma" && export AWS_REGION="eu-west-2"

clear

# outputDirLambdas="LambdasZIPToPersonal"
# cd lambdas
cd ~/Downloads
for file in *.js; do 
     aws lambda create-function --function-name ldma-dev-${file%.*} --runtime nodejs6.10 --role arn:aws:iam::$sharedldmaAWSAccountID:role/ldma-dev-eu-west-2-lambdaRole --handler ${file%.*}.${file%.*} --zip-file fileb://${file%.*}.zip  --region=eu-west-2; 
     export statementIdUUID=`uuidgen`
     aws lambda add-permission --function-name ldma-dev-${file%.*}  --principal apigateway.amazonaws.com --statement-id  $statementIdUUID --action lambda:* --region=eu-west-2
done


# En caso de querer importar toda la api si no estuviera.
aws apigateway import-rest-api --fail-on-warnings --body 'file://dev-ldma-API.json' --region=eu-west-2


