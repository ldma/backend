
# ESTE SCRIPT TE DESCARGA TODO LO QUE TENEMOS DE LA CUENTA COMPARTIDA LDMA (API + Lambdas)
# Precauciones: Primero debes ejecutar el "fillAWSConfigDeploy" para coger los datos válidos de la cuenta de AWS (id personal y id API)
#uso:
#sh 02FromPersonalAWSToCommitsIntoBranchRepo.sh <descripcion_del_deploy> 
#sh 02FromPersonalAWSToCommitsIntoBranchRepo.sh "description of deployment"

# Consideraciones: Tener limpia la carpeta de downloads (ya que se bajarán aquí)
# Las lambdas estarán en Downloads y la api gateway como "dev-ldma-API.json" 
# 

export AWS_PROFILE="ldma" && export AWS_REGION="eu-west-2"
clear

function pause(){
   read -p "$*"
}

function getCredentials(){
    personalAWSAccountID=$(jq '.personalAWSAccountID' AWSConfigDeploy.json)
    sharedldmaAWSAccountID=$(jq '.sharedldmaAWSAccountID' AWSConfigDeploy.json)
    personalApiID=$(jq '.personalApiID' AWSConfigDeploy.json)
    sharedldmaApiID=$(jq '.sharedldmaApiID' AWSConfigDeploy.json)
    personalAWSAccountID=${personalAWSAccountID//[!0-9]/}
    sharedldmaAWSAccountID=${sharedldmaAWSAccountID//[!0-9]/}
    personalApiID=${personalApiID//[!0-9&!a-z]/}
    sharedldmaApiID=${sharedldmaApiID//[!0-9&!a-z]/}
}
getCredentials

# #Exporting lambdas of personal account
touch urlsLambdas
aws lambda list-functions --region=eu-west-2 > list_functions.json
countItems=$(jq '.Functions | length' list_functions.json)
echo "$countItems lambdas to get of your AWS personal account"
for (( c=0; c<$countItems; c++ ))
do  
lambdaName=$(jq '.Functions['$c'].FunctionName' list_functions.json) 
lambdaName="${lambdaName//'"'}"

aws lambda get-function --function-name $lambdaName --region='eu-west-2' > data_lambda.json
    urlLambda=$(jq '.Code.Location' data_lambda.json)
    urlLambda="${urlLambda//'"'}"  
    echo "$lambdaName : $urlLambda" >> urlsLambdas
    open -a /Applications/Safari.app $urlLambda 
done

# # #adding zip part

# # #TODO: entrar dentro de cada carpeta y comprimir todo lo que haya al nombre correcto.

 cd ~/Downloads/ 
for i in $(ls -d */); do 
    nameCorrected=$(echo "$i" | cut -d '-' -f1-3)
    nameCorrected2=$(echo "$nameCorrected" | cut -d '/' -f5)
    nameCorrectedPath=$(echo "$D" | cut -d '/' -f5)
    nameLambdaFolderWithDependenciesCorrected=$(echo ${i%%/} | cut -d '-' -f3)
    mv ${i%%/} $nameLambdaFolderWithDependenciesCorrected
    zip -r $nameLambdaFolderWithDependenciesCorrected.zip ./$nameLambdaFolderWithDependenciesCorrected
    cp ./$nameLambdaFolderWithDependenciesCorrected/*.js .
done


# # # Export api gateway

# #  clear
 apiSwagerJson="dev-ldma-API.json" 
 apiSwagerJsonFromAws="dev-ldma-API.json-e"
 stageName="dev"
 cd - 
 touch $apiSwagerJson 
 

# se debe hacer un deploy antes (Aquí el script lo hace por nosotros)!!
# aws apigateway create-deployment --rest-api-id $sharedldmaApiID --stage-name 'dev' --stage-description 'Development Stage' --description $1 --region=eu-west-2
 aws apigateway get-export --parameters extensions='integrations' --rest-api-id $sharedldmaApiID --stage-name $stageName --export-type swagger $apiSwagerJson --region=eu-west-2

#rm $apiSwagerJsonFromAws
