# #!/bin/bash
# # invocación :
# ejecutar el sh fillAWSConfigDeploy.sh para obtener datos necesarios de AWS para el despliegue y la importación/exportación 
#sh 01... <dependencia> : ejemplo: sh 001FromAwsLdmaToPersonalAWS.sh "uuid" en caso de tener, sino no poner nada
# la dependencia debe estar como carpeta dentro de "lambdas" (/lambdas/uuid)
# Sigue el esquema del repo (Carpeta de lambdas (createActivity.js,etc...) + API: dev-ldma-API.json))
clear
export AWS_PROFILE="personal" && export AWS_REGION="eu-west-2"

function pause(){
   read -p "$*"
}

function getCredentials(){
    personalAWSAccountID=$(jq '.personalAWSAccountID' AWSConfigDeploy.json)
    sharedldmaAWSAccountID=$(jq '.sharedldmaAWSAccountID' AWSConfigDeploy.json)
    personalApiID=$(jq '.personalApiID' AWSConfigDeploy.json)
    sharedldmaApiID=$(jq '.sharedldmaApiID' AWSConfigDeploy.json)
    personalAWSAccountID=${personalAWSAccountID//[!0-9]/}
    sharedldmaAWSAccountID=${sharedldmaAWSAccountID//[!0-9]/}
    personalApiID=${personalApiID//[!0-9&!a-z]/}
    sharedldmaApiID=${sharedldmaApiID//[!0-9&!a-z]/}
}

getCredentials

touch urlsLambdas
aws lambda list-functions --region=eu-west-2 > listLambdasAwsPersonal.json
countItems=$(jq '.Functions | length' listLambdasAwsPersonal.json)
echo You have $countItems lambda in $AWS_REGION.
echo "Deleting All lambdas...\n See the 'eraseAllLambdaPersonal.json' for more details" 
if [ $countItems -gt "0" ]; then
    for (( c=0; c<$countItems; c++ )) do
        lambdaName=$(jq '.Functions['$c'].FunctionName' listLambdasAwsPersonal.json) 
        lambdaName="${lambdaName//'"'}"
        aws lambda delete-function --function-name $lambdaName --region eu-west-2
        echo "lambda $(($c+1)) -> $lambdaName deleted!"
        done
fi
rm urlsLambdas 

pause 'All Lambdas deleted! Press any key to import all lambdas to your personal AWS account...'


clear
# import all the lambdas of the folder and upload to Ldma shared account
outputDirLambdas="LambdasZIPToPersonal"

if [ -d $outputDirLambdas]; then  
echo "Directory already created."
else 
mkdir $outputDirLambdas
echo "LambdaZIP directory created!"
fi

# Para hacer similar al Path del repo de lambdas
cd lambdas

for file in *.js; do 

    result=$(grep -i "$1" $file)
    if [ -z "$result" ]; then
        echo "No dependency '$1' found in ${file%.*}"
        zip ../$outputDirLambdas/${file%.*}.zip $file
    else
        echo "'$1' found in ${file%.*}"
        zip -r ../$outputDirLambdas/${file%.*}.zip $file $1
    fi;
done
pause 'All Lambdas Zipped! Press any key to create and add permission to Lambdas...'
# upload all the Ziplambdas with permissions
for file in *.js; do 
    aws lambda create-function --function-name ldma-dev-${file%.*} --runtime nodejs6.10 --role arn:aws:iam::$personalAWSAccountID:role/ldma-development-eu-west-2-lambdaRole --handler ${file%.*}.${file%.*} --zip-file fileb://../$outputDirLambdas/${file%.*}.zip  --region=eu-west-2 
    export statementIdUUID=`uuidgen`
    aws lambda add-permission --function-name ldma-dev-${file%.*}  --principal apigateway.amazonaws.com --statement-id  $statementIdUUID --action lambda:* --region=eu-west-2
done
pause 'All Lambdas uploaded! Press any key to import the api...'
cd ..
# import API gateway


# cambiar el personalAWSAccountID 
sed -i -e 's/096593837768/295228814912/g' dev-ldma-API.json

aws apigateway import-rest-api --fail-on-warnings --body 'file://dev-ldma-API.json' --region=eu-west-2 > deployInfoAPI.json
#write api id into AWSConfigDeploy.json
    personalApiID=$(jq '.id' deployInfoAPI.json)
    personalApiID=${personalApiID//[!0-9&!a-z]/}
    cat AWSConfigDeploy.json | jq --arg personalApiID $personalApiID 'to_entries | 
       map(if .key == "personalApiID" 
          then . + {"value": $personalApiID}
          else . 
          end
         ) | 
      from_entries' > AWSConfigDeployTmp.json
mv AWSConfigDeployTmp.json AWSConfigDeploy.json

echo 'Done! All API was successfully uploaded to your personal AWS Account'
