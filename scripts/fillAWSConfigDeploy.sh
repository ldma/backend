#!/bin/bash
export AWS_PROFILE="personal" && export AWS_REGION="eu-west-2"
aws sts get-caller-identity > personalAWSAccountID.json
export AWS_PROFILE="ldma" && export AWS_REGION="eu-west-2"
aws sts get-caller-identity > sharedldmaAWSAccountID.json

personalAccountID=$(jq '.Account' personalAWSAccountID.json)
sharedLDMAAccountID=$(jq '.Account' sharedldmaAWSAccountID.json)
personalAccountID=${personalAccountID//[!0-9]/}
pathClonedLdmaRepo=$("pwd")
pathClonedLdmaRepo=$pathClonedLdmaRepo



cat AWSConfigDeploy.json | 
  jq --arg personalAccountID $personalAccountID 'to_entries | 
       map(if .key == "personalAWSAccountID" 
          then . + {"value": $personalAccountID}
          else . 
          end
         ) | 
      from_entries' > AWSConfigDeployTmp.json
mv AWSConfigDeployTmp.json AWSConfigDeploy.json



