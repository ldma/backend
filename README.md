# LDMA Backend

Backend For the Final Project of MOBILE STARTUP ENGINEERING BOOTCAMP IV

Devoloped with AWS Lambda, AWS API Gateway, AWS Cognito, AWS S3 and AWS DynamoDB

![](https://s3.eu-west-2.amazonaws.com/com.ldma.files/aws.png)

## Path

https//ozk6fvd056.execute-api.eu-west-2.amazonaws.com/prod/apiv1/

Routed with AWS API Gateway

## Security

Security based on AWS Cognito.

## List of TODO's

- Implement Cognito auth to all methods
- Dynamodb pagination to all methods (Just implemented on list activity)
- Filtering activities by polygon gps region
- Filtering services by polygon gps region
- Massive upload of Company's employees
- Chat within members and activity members
- Push notifications
- Mailing notifications (invited to activity)

## ENDPOINTS

### /activity

- **GET  /apiv1/activity**
- **POST  /apiv1/activity**
- **GET  /apiv1/activity/{id}**
- **DELETE  /apiv1/activity/{id}**

### /company

- **GET  /apiv1/company**
- **POST  /apiv1/company**
- **DELETE  /apiv1/company/{id}**
- **PUT  /apiv1/company/{id}**

### /companybyid

- **GET  /apiv1/companybyid/{id}**

### /department

- **GET  /apiv1/department**
- **POST  /apiv1/department**
- **GET  /apiv1/department/{id}**
- **DELETE  /apiv1/department/{id}**

### /employee

- **GET  /apiv1/employee**
- **POST  /apiv1/employee**
- **GET  /apiv1/employee/{id}**
- **DELETE  /apiv1/employee/{id}**

### /service

- **GET  /apiv1/service**

### /user

- **GET  /apiv1/user**
- **POST  /apiv1/user**
- **GET  /apiv1/user/{id}**
- **DELETE  /apiv1/user/{id}**
- **PUT  /apiv1/user/{id}**

## TEAM

- [Eric Risco][1]
- [Begoña Hormaechea][2]
- [Alberto Galera][3]
- [Paco Cardenal][4]
- [Eugenio Barquín][5]

[1]: https://github.com/eriscoand
[2]: https://github.com/begohorma
[3]: https://github.com/albertgs
[4]: https://github.com/pacocardenal
[5]: https://github.com/ebarquin